#include <stdio.h>

int main()
{   
    //Criando variaveis que vão receber a quantidade de elemento.
    
    int quant, razao;
    int contador; 
    int prog = 0;

    //Recebendo e lendo a quantidade de elementos desejada pelo usuário.
    printf("digite a quantidade de elementos da progressao: ");
    scanf("%d", &quant);

    //Recebendo a razão desejada pelo usuário.
    printf("digite a razao da progressao: ");
    scanf("%d", &razao);
    
    //Toda progressão aritmética começa por "0".
    printf("0");

    //Criando um for para contar a quantidade de elementos e fazer a adição da razão.
    for(contador = 2; contador <= quant; contador++)
    {
        prog = prog + razao;
        printf("%d", prog);
    }
return 0;
}