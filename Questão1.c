#include <stdio.h>
#include <stdlib.h>

int main(){

    char menu[] = "Ola. Tudo bem? Digite: um numero entre 1 e 9, ou 0 para sair: ";
    int escolha = -1;
    while (escolha != 0){
        printf("\n%s", menu);
        scanf("%d", &escolha);
        switch (escolha) {
            case 0:
                printf("Tchau.\n");
                return 0;
            case 1:          
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                printf("\nVc digitou %d", escolha);
                break;
            default:
                printf("\nVc digitou opcao inexistente!");
                break;
        }
        

    }
    return 0;
}