#include <stdio.h>

int main(){
	
    // Criando variaveis que vão receber a quantidade de elementos
    int quant, razao;
    int contador;
    int prog = 1;
    
    //recebendo e lendo a quantidade de elementos desejada pelo usuário
    printf("digite a quantidade de elementos da progressao: ");
    scanf("%d", &quant);

    //recebendo e lendo a razão desejada pelo usuário
    printf("digite a razao da progressao: ");
    scanf("%d", &razao);

    //toda progressão geométrica começa pelo numero 1
    printf("1\n");
    
    //criando um for para para contar a quantidade de elementos e para fazer a multiplicação da razão  
    for(contador = 2; contador <= quant; contador++)
    {
        prog = prog * razao;

        printf("%d\n", prog);
    }
return 0;
}